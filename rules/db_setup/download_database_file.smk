from utils.db_handling.db_access import DbRequirementsAccessor


rule download_using_direct_dl:
    # changed protected to directory
    output:
        protected("{database}/{version}/direct_dl/{filename}")
    message:
        f"downloading file: {'{output[0]}'}"
    version: "0.1"
    params:
        url=lambda w: DbRequirementsAccessor(w.database, w.version).get_url_for_file(w.filename)
    shell:
        """
        wget -nv -O "{output}" "{params.url}"
        """

rule download_using_emapper_v1_dl:
    """
    downlad eggnog databases for emapper version 1, eggnong database v4
    params for download_eggnong_db.py :
    -y: assume yes to all questions
    -f: forces download even if file exists
    --data-dir: directory for download DB
    """
    output:
        # protecting emapper data results in snakemake crash, probably due to large number of files
        directory("eggnog/{version, 4.*}/emapper_dl/{directory}")
    message:
        f"downloading file using emapper: {'{output[0]}'}"
    conda:
        "../../envs/emapper_v1.yaml"
    shell:
        """
        mkdir -p "{output[0]}";
        download_eggnog_data.py -y -f --data_dir "{output[0]}" euk bact arch viruses;
        """


rule download_using_emapper_v2_dl:
    """
    downlad eggnog databases for emapper version 2, eggnong database v5
    params for download_eggnong_db.py :
    -y: assume yes to all questions
    -f: forces download even if file exists
    --data-dir: directory for download DB
    (-F: download novel families of eggnong database v6)
    """
    output:
        directory("eggnog/{version, 5.*}/emapper_dl/{directory}")
    message:
        f"downloading file using emapper: {'{output[0]}'}"
    conda:
        "../../envs/emapper_v2.yaml"
    shell:
        """
        mkdir -p "{output[0]}";
        download_eggnog_data.py -y -f --data_dir "{output[0]}";
        """
