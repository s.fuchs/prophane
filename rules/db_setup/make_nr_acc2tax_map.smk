def linked_db_files(wildcards):
    db_obj = get_db_object_for_dbtype_and_version("ncbi_nr", wildcards.db_version)
    taxdump_map = db_obj.get_tax2annot_map_file(relative=True)
    lst_acc2tax_files = db_obj.get_list_of_acc2tax_files(relative=True)
    lst_all = [taxdump_map] + lst_acc2tax_files
    return lst_all


rule make_nr_acc2tax_map:
    input:
        linked_db_files
    output:
        "ncbi_nr/{db_version}/processed/acc2tax.map.gz",
        "ncbi_nr/{db_version}/processed/acc2tax.unmapped.log.gz"
    message:
        "generating taxmap {output[0]}"
    benchmark:
        "ncbi_nr/{db_version}/processed/acc2tax.map.gz.benchmark.txt"
    resources:
        mem_mb=1024*3
    params:
        db_obj=lambda wildcards: get_db_object_for_dbtype_and_version("ncbi_nr", wildcards.db_version)
    run:
        params.db_obj.create_acc2tax_map(
            taxdump_in=input[0], l_acc2taxid_in=input[1:], nr_out=output[0], nr_missing_out=output[1])
