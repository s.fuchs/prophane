rule gunzip_directly_downloaded_db_resource_file:
  input:
    "{db_dir}/direct_dl/{fname}.gz"
  output:
    "{db_dir}/processed/{fname}.unzipped"
  message:
      "unpacking file {input[0]}"
  version: "0.13"
  shell:
    '''
    gunzip -k --stdout {input[0]} > {output[0]}
    '''
