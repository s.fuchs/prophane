from utils.db_handling.db_access import get_db_object_for_dbtype_and_version

rule make_uniprot_tax2annot_map:
    input:
        up_tax_txt=lambda w: get_db_object_for_dbtype_and_version(
            "uniprot_tax",
            w.db_version
        ).get_taxmap_txt(relative=True)
    output:
        "uniprot_tax/{db_version}/processed/tax2annot.map.gz",
        "uniprot_tax/{db_version}/processed/tax2annot.unmapped.log.gz"
    message:
        "generating taxmap {output[0]}"
    benchmark:
        "uniprot_tax/{db_version}/processed/tax2annot.map.gz.benchmark.txt"
    resources:
        mem_mb=1024*3
    version:
        "0.1"
    params:
        db_obj=lambda w: get_db_object_for_dbtype_and_version("uniprot_tax", w.db_version)
    shell:
        "gzip -k --stdout {input.up_tax_txt} > {output[0]}; touch {output[1]}"
