def input_make_dmnd_db(wildcards):
    db_obj = get_db_object_for_dbtype_and_version(wildcards.db_type, wildcards.db_version)
    lst_fasta_gz = db_obj.get_fasta_gz_file_list(relative=True)
    return lst_fasta_gz

rule make_dmnd_db:
  input:
    fasta_gz_list=input_make_dmnd_db
  output:
    dmnd_db="{db_type}/{db_version}/processed/diamond_db.dmnd",
    # temporary dir is placed in DB folder as its size can exceed multiple GBs
    temp_dir=temp(directory("{db_type}/{db_version}/processed/diamond_db_TMP"))
  message:
    "preparing diamond db for first use: {output[0]}"
  log:
    "{db_type}/{db_version}/processed/diamond_db.dmnd.log"
  version: "0.12"
  conda:
    "../../envs/diamond.yaml"
  threads: 10
  benchmark:
    "{db_type}/{db_version}/processed/diamond_db.dmnd.benchmark.txt"
  resources:
    mem_mb=1024*5
  shell:
    '''
    mkdir {output.temp_dir}
    zcat {input.fasta_gz_list} | diamond makedb -t {output.temp_dir} --db {output.dmnd_db} --threads {threads} > {log} 2>&1
    '''
