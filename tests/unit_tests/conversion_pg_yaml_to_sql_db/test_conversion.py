import os
import shutil

import pytest

from tests.test_integration import compare_sql_databases
from utils.protein_group_sql_db import ProteinGroupSqlDbInteractor

path_abs = os.path.abspath('tests/unit_tests/conversion_pg_yaml_to_sql_db/')

def get_tmppdir(tmpdir, filename):
    p = tmpdir.join(filename)
    return p

test_data = [('tests/unit_tests/conversion_pg_yaml_to_sql_db/job/without_spectra/protein_groups.yaml',
              'tests/unit_tests/conversion_pg_yaml_to_sql_db/expected_results/protein_groups_db_without_spectra.sql'),
             ('tests/unit_tests/conversion_pg_yaml_to_sql_db/job/with_spectra/protein_groups.yaml',
              'tests/unit_tests/conversion_pg_yaml_to_sql_db/expected_results/protein_groups_db_with_spectra.sql')]

@pytest.mark.parametrize("pg_yaml, expected_db", test_data)
def test_conversion_pg_yaml(tmpdir, pg_yaml, expected_db):
   # output_db = tmpdir + '/job/without_spectra/protein_groups_db.sql'
   pg_yaml_tmpdir = get_tmppdir(tmpdir,  'protein_groups.yaml')
   pg_db_tmpdir = get_tmppdir(tmpdir,  '/protein_groups_db.sql')
   shutil.copy(pg_yaml, pg_yaml_tmpdir)
   db_obj = ProteinGroupSqlDbInteractor(pg_yaml_tmpdir)
   compare_sql_databases(pg_db_tmpdir, expected_db)
