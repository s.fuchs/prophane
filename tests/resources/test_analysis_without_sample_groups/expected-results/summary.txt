level	#pg	members_count	min_seqlen	max_seqlen	min_rel_pairw_ident	max_rel_pairw_ident	members_identifier	task_0::fun_from_my_file::level1	task_0::fun_from_my_file::level2	task_0::fun_from_my_file::level3	raw_quant::sample A::replicate 1	raw_quant_sd::sample A::replicate 1	quant::sample A::replicate 1	quant_sd::sample A::replicate 1	task_0::fun_from_my_file::level1::lca_support	task_0::fun_from_my_file::level2::lca_support	task_0::fun_from_my_file::level3::lca_support
group	0	3	81	81	100.0	100.0	KZK33282.1;NP_268346.1;WP_003131952.1	Bacteria	Firmicutes	Bacilli	1.2	-	0.21051180751059884	-	3/3	3/3	3/3
member	0	-	-	-	-	-	KZK33282.1	Bacteria	Firmicutes	Bacilli
member	0	-	-	-	-	-	NP_268346.1	Bacteria	Firmicutes	Bacilli
member	0	-	-	-	-	-	WP_003131952.1	Bacteria	Firmicutes	Bacilli
group	1	3	393	494	14.03	27.0	PKC80086.1;SUC62851.1;XP_963622.1	various	various	various	1.0	-	0.02876426519628628	-	(1)/3	(1)/3	(1)/3
member	1	-	-	-	-	-	PKC80086.1	Bacteria	Actinobacteria	Bifidobacteriales
member	1	-	-	-	-	-	SUC62851.1	unclassified	unclassified	unclassified
member	1	-	-	-	-	-	XP_963622.1	Eukaryota	Ascomycota	Sordariomycetes
group	2	1	-	-	-	-	WP_000665196.1	Bacteria	Proteobacteria	various	3.0	-	0.2631397593882486	-	1/1	1/1	1/1
member	2	-	-	-	-	-	WP_000665196.1	Bacteria	Proteobacteria	Gammaproteobacteria
member	2	-	-	-	-	-	WP_000665196.1	Bacteria	Proteobacteria	Gammaproteobacteria group
group	3	2	204	458	9.72	9.72	sp|Q197F8;sp|Q6GZX0	unclassified	unclassified	unclassified	1.0	-	0.031025211805601357	-	0/0	0/0	0/0
member	3	-	-	-	-	-	sp|Q197F8	unclassified	unclassified	unclassified
member	3	-	-	-	-	-	sp|Q6GZX0	unclassified	unclassified	unclassified
group	4	3	212	629	4.08	96.6	sp|Q6GZU4;tr|A0A2S1TQ58;tr|G8IHY6	unclassified	unclassified	unclassified	3.0	-	0.06777208429395273	-	0/0	0/0	0/0
member	4	-	-	-	-	-	sp|Q6GZU4	unclassified	unclassified	unclassified
member	4	-	-	-	-	-	tr|A0A2S1TQ58	unclassified	unclassified	unclassified
member	4	-	-	-	-	-	tr|G8IHY6	unclassified	unclassified	unclassified
group	5	1	-	-	-	-	sp|Q6GZV2	unclassified	unclassified	unclassified	9.0	-	0.3347799033054681	-	0/0	0/0	0/0
member	5	-	-	-	-	-	sp|Q6GZV2	unclassified	unclassified	unclassified
group	6	1	-	-	-	-	tr|A0A126QUZ5	unclassified	unclassified	unclassified	1.0	-	0.06400696849984425	-	0/0	0/0	0/0
member	6	-	-	-	-	-	tr|A0A126QUZ5	unclassified	unclassified	unclassified
