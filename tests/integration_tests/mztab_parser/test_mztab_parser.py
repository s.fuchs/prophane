import pytest
import hashlib

from tests.test_integration import compare_sql_databases
from utils.search_result_parsing import ProteomicSearchResultParser


def md5sum(filename):
    data = open(filename, 'rb').read()
    return hashlib.md5(data).hexdigest()


test_data = [('tests/integration_tests/mztab_parser/job/pd_mztab_example.mztab',
              'pd_example_pg_db.sql',
              'tests/integration_tests/mztab_parser/expected_results/pd_example_pg_db.sql',
              r'REV_$'),
             ('tests/resources/test_parse_mztab/job_dir/maxquant_mzTab_example.mztab',
              'maxquant_pg_db.sql',
              'tests/resources/test_parse_mztab/expected-results/protein_groups_db.sql',
              '^REV_|^CON_')]
style = 'styles/mztab_1_0_0.yaml'
required_sample_descriptors = []


@pytest.mark.parametrize("mztab, output_db, correct_pg_db, decoy_regex", test_data)
def test_pg_db_from_mztab(tmpdir, mztab, output_db, correct_pg_db, decoy_regex):
    test_result = tmpdir / output_db
    protein_parser = ProteomicSearchResultParser(mztab, style, test_result, decoy_regex,
                                                 required_sample_descriptors)
    protein_parser.create_pg_sql_db()
    compare_sql_databases(test_result, correct_pg_db)
