import os

from utils.config import validate_config_and_set_defaults
from utils.vars import PROPHANE_DIR
from collections import OrderedDict

CONFIG = {
    "db_base_dir": "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/test_dbs",
    "general": OrderedDict([("job_name", "Test"), ("job_comment", "none")]),
    "input": OrderedDict(
        [
            (
                "search_result",
                "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/job_dir/input/protein_report.xls",
            ),
            (
                "report_style",
                "/home/hennings/Projects/prophane/styles/scaffold_4_8_x.yaml",
            ),
            (
                "fastas",
                [
                    "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/job_dir/input/target_db.fasta"
                ],
            ),
            ("taxmaps", []),
        ]
    ),
    "output_dir": "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/job_dir/",
    "seqdb": [
        "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/test_dbs/skip/skip.fa"
    ],
    "taxmaps": [
        "/tmp/pytest-of-hennings/pytest-44/test_full_analysis_dryrun0/test_dbs/skip/skip.map"
    ],
    "tasks": [
        OrderedDict(
            [
                ("prog", "diamond blastp"),
                ("type", "taxonomic"),
                ("shortname", "tax_from_nr_20180808_qcover90_diamond"),
                ("db_type", "ncbi_nr"),
                ("params", OrderedDict([("evalue", 0.01), ("query-cover", 0.9)])),
            ]
        ),
    ],
    "decoy_regex": "DECOY$",
    "quant_method": "max_nsaf",
    "sample_groups": OrderedDict([("sample", ["sample A::replicate 1"])]),
}


def test_validate_config_and_set_defaults():
    test_dict = CONFIG
    schema = os.path.join(PROPHANE_DIR, "schemas", "config.schema.yaml")
    test_dict = validate_config_and_set_defaults(test_dict, schema)
    assert "taxlevel" in test_dict
    assert "lca" in test_dict
