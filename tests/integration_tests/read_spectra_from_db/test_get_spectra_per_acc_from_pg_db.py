from collections import defaultdict

import pytest
from utils.protein_group_sql_db import ProteinGroupSqlDbInteractor

pg_db = 'tests/unit_tests/read_spectra_from_db/protein_groups_db.sql'
pd_result = defaultdict(set)
pd_result['BPBMOCPF_00058'] = set()
pd_result['LPLDNAKH_00307'] = {'ms_run[14]:scan=21060', 'ms_run[4]:scan=21755', 'ms_run[24]:scan=21676'}
pd_result['LPLDNAKH_06997'] = set()
pd_result['BPBMOCPF_00062'] = {'ms_run[20]:scan=30174', 'ms_run[20]:scan=16709', 'ms_run[7]:scan=28288', 'ms_run[24]:scan=29222',
                               'ms_run[11]:scan=28665', 'ms_run[20]:scan=30146', 'ms_run[21]:scan=7777', 'ms_run[19]:scan=29573',
                               'ms_run[24]:scan=7969', 'ms_run[5]:scan=28896', 'ms_run[13]:scan=28348', 'ms_run[19]:scan=7538',
                               'ms_run[1]:scan=8188', 'ms_run[8]:scan=28186', 'ms_run[15]:scan=7808', 'ms_run[24]:scan=29304',
                               'ms_run[9]:scan=29069', 'ms_run[18]:scan=36381', 'ms_run[2]:scan=28770', 'ms_run[17]:scan=29917'}
pd_result['BPBMOCPF_00095'] = {'ms_run[22]:scan=8340', 'ms_run[13]:scan=8404', 'ms_run[18]:scan=8437', 'ms_run[5]:scan=8507',
                               'ms_run[16]:scan=7676', 'ms_run[7]:scan=8035'}
pd_result['BPBMOCPF_00143'] = {'ms_run[2]:scan=14021', 'ms_run[12]:scan=9866', 'ms_run[3]:scan=9361', 'ms_run[11]:scan=13732',
                               'ms_run[11]:scan=4519', 'ms_run[1]:scan=4456', 'ms_run[20]:scan=12813', 'ms_run[5]:scan=29225',
                               'ms_run[20]:scan=11522', 'ms_run[2]:scan=10033', 'ms_run[23]:scan=11053', 'ms_run[6]:scan=9764',
                               'ms_run[22]:scan=9649', 'ms_run[2]:scan=4771', 'ms_run[9]:scan=9703', 'ms_run[17]:scan=11162',
                               'ms_run[1]:scan=13503', 'ms_run[15]:scan=8773'}


def test_spectra_from_db():
    # proteome_discoverer_example.yaml
    pg_db = 'tests/integration_tests/read_spectra_from_db/protein_groups_db.sql'
    accs_to_spectra_dict = ProteinGroupSqlDbInteractor(pg_db).get_acc_to_spectra_dict()
    assert pd_result == accs_to_spectra_dict