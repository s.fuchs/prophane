import pytest
import hashlib

from tests.test_integration import compare_sql_databases
from utils.search_result_parsing import ProteomicSearchResultParser


def md5sum(filename):
    data = open(filename, 'rb').read()
    return hashlib.md5(data).hexdigest()


def get_tmppdir(tmpdir, filename):
    p = tmpdir.join(filename)
    return p


test_data = [('tests/integration_tests/pd_parser/job/proteome_discoverer_test.xlsx',
              'protein_groups_1.db',
              'tests/integration_tests/pd_parser/expected_results/result1_protein_groups_db.sql'),
             ('tests/integration_tests/pd_parser/job/PD_test2.xlsx',
              'protein_groups_2.db',
              'tests/integration_tests/pd_parser/expected_results/result2_protein_groups_db.sql'),
             ('tests/integration_tests/pd_parser/job/error_test.xlsx',
              'protein_groups_error_test.db',
              'tests/integration_tests/pd_parser/expected_results/protein_groups_error_test_db.sql')]
decoy_exp = 'REV_$'
style = 'styles/proteome_discoverer_2_5.yaml'
required_sample_descriptors = []

@pytest.mark.parametrize("xlsx, output_db, correct_pg_db", test_data)
def test_pg_db_from_pd(tmpdir, xlsx, output_db, correct_pg_db):
    test_result = get_tmppdir(tmpdir, output_db)
    protein_parser = ProteomicSearchResultParser(xlsx, style, test_result, decoy_exp,
                                                 required_sample_descriptors)
    protein_parser.create_pg_sql_db()
    compare_sql_databases(test_result, correct_pg_db)