import sys
from collections import OrderedDict
import re

import numpy as np
import pandas as pd

from utils.search_result_parsing.helper_classes import ProteomicSearchResultReaderBaseClass, InputFile


class PDParser(ProteomicSearchResultReaderBaseClass):
    """
    parsing of the xlsx protein group proteome discoverer output:
    1. read excel file into pandas dataframe
    dataframe structure:
        header: used for counting samples (header elements starting with "Found in Sample" are counted)
        protein_group_header: with elements "Master", "Accession" and "Abundance...: sample "
        protein_group_rows: all rows until next protein_group_header. Contain following information:
         [acc, master-protein/master-candidate/None, abundance values per sample]
    2. determine samples -> counting of "Found in Sample" in df header -> sample names = "F+count_nb+1"
    3. determine columns used for abundance: first protein group header (=row 2) must match one of this terms
            ['Abundance:', 'Abundances (Normalized):', 'Abundances (Scaled):']
            If multiple matching Abundance terms are present, only one is selected, in the order above
    4. determine column position of needed columns (Master, Accession, Abundances) and reduce df to these columns
    5. split into df for every protein group:
            determine position of first protein group header and last protein group row in df
    6. get quant values: if master-protein is present (mostly):
        abundances taken from the master-protein row of a protein group,
        if no abundance entries for all samples in the master-protein row --> protein group discarded
        if master_protein not present: abundances = mean(abundances of all proteins of this group, ignoring 0 values)
    """

    def check_and_adapt_accessions(self, df_standard):
        for accessions in df_standard.proteins:
            for i, accession in enumerate(accessions):
                if len(accession.split('|')) > 2:
                    accessions[i] = accession.split('|')[1]
        return df_standard

    def get_sample_list(self, pg_header_list, pos_abundance_list):
        """
        :param pg_header_list: list of header elements of the first protein group sub header
        :param pos_abundance_list: [ (column number (type: int), 'Abundance ...' (type:str))
        """
        samples = []
        for pos, a in pos_abundance_list:
            sample_name = re.search(r'F\d+', pg_header_list[pos]).group()
            samples.append(sample_name)
        return samples

    def get_abundance_columns(self, second_df_row):
        """
        Find positions of Abundances of different samples, if multiple different abundance methods exists, following
        abundances are used in this order: Abundance, Abundances (Scaled), Abundances (Normalized)
        :param second_df_row: second row of dataframe
        :return start_pos_abundance: list of tuples [(index (=column number) of the abundance values, column content)
            e.g. [(12, "Abundances F1: Sample"), (13, "Abundances F2: Sample"), ...]
        """
        start_pos_abundance_scales = []
        start_pos_abundance_normalized = []
        start_pos_abundance = []
        for i, elem in enumerate(second_df_row):
            try:
                if elem.startswith('Abundance:'):
                    start_pos_abundance.append((i, elem))
                elif elem.startswith('Abundances (Normalized):'):
                    start_pos_abundance_normalized.append((i, elem))
                elif elem.startswith('Abundances (Scaled):'):
                    start_pos_abundance_scales.append((i, elem))
            except AttributeError:
                continue
        if len(start_pos_abundance) > 0:
            return start_pos_abundance
        elif len(start_pos_abundance_scales) > 0:
            return start_pos_abundance_scales
        elif len(start_pos_abundance_normalized) > 0:
            return start_pos_abundance_normalized
        else:
            raise ValueError(
                "No abundance information in Proteome Discoverer file. Abundance must match one of the following:"
                " Abundance: sample, Abundances (Scaled): sample, Abundances (Normalized): sample"
            )

    def get_columns_indices(self, second_row):
        """
        :param second_row: first header_row of "protein group sub_df"
        :return column_to_index_dict: {column name (Master OR Accession): index of column
        """
        column_to_index_dict = OrderedDict()
        for j, elem in enumerate(second_row):
            if elem == 'Master':
                column_to_index_dict['Master'] = j
            elif elem == 'Accession':
                column_to_index_dict['Accession'] = j
        if "Accession" not in column_to_index_dict.keys():
            raise AssertionError('Accession column is missing in protein groups!')
        if 'Master' not in column_to_index_dict.keys():
            print(
                'Master column is not available. Instead of using abundance values of the master protein, '
                'non-zero abundances of all protein group members are averaged.',
                file=sys.stderr
            )
        return column_to_index_dict

    def reduce_full_df_to_abundance_df(self, df, pos_abundance_list):
        column_to_index_dict = self.get_columns_indices(df.iloc[[1]].values.tolist()[0])
        abundance_names = [elem[1] for elem in pos_abundance_list]

        column_names = [k for k in column_to_index_dict.keys()]
        column_names.extend(abundance_names)

        columns_indices = [v for v in column_to_index_dict.values()]
        columns_indices.extend([elem[0] for elem in pos_abundance_list])

        abundance_df = df.iloc[:, columns_indices]
        abundance_df.columns = column_names
        abundance_df = abundance_df.fillna(0)
        return abundance_df

    def get_master_row_index(self, pg_df):
        try:
            i = pg_df[pg_df['Master'] == 'Master Protein'].index.values[0]
        except IndexError:
            i = None
        return i

    def split_df_into_group_dfs(self, df):
        # find row index for all entries belonging to the "outer" df (#PSMs column outer df = Accession column inner df,
        # one protein group "sub-df" is below
        df_new_group = df[df['Accession'].apply(lambda x: isinstance(x, int))]
        # split into one df per protein group first row= "outer" row +2 (skipping sub-header), last row = next index -1
        split_indices = []
        for i in range(len(df_new_group.index) - 1):
            split_indices.append((df_new_group.index[i] + 2, df_new_group.index[i + 1]))
        # add last protein group
        split_indices.append((df_new_group.index[-1] + 2, len(df)))
        # split by row_numbers
        dfs_per_protein_group = [df.iloc[n:m, :].reset_index(drop=True) for n, m in split_indices]
        return dfs_per_protein_group

    def create_dict_from_reduced_df(self, df, sample_list):
        """
        :param df: dataframe with columns: (Master), Accession, Abundance sample 1, Abundance sample 2 ...
        :param sample_list: List of sorted sample names ["F1", "F10", "F11", "F2", ...]
        : return samples, abundance, proteins: lists, list, list of lists
            e.g. ["F1", "F2"], [100, 45.5], [["acc1", "acc2"], ["acc1", "acc2"]]
        """
        with_master_column = 'Master' in df.columns
        col_index_start_abundances = [i for i, name in enumerate(df.columns) if 'Abundance' in name][0]
        samples, proteins, abundance = [], [], []
        pg_without_information = 0
        pg_without_abundances = []
        dfs_per_protein_group = self.split_df_into_group_dfs(df)

        for pg, pg_df in enumerate(dfs_per_protein_group):
            if len(pg_df) == 0:
                pg_without_information += 1
                continue
            # accs protein group
            accs_per_pg = list(pg_df['Accession'])
            if with_master_column:
                # master protein accession to first position (not possible, sorted by alphabet)
                # accs_per_pg.insert(0, accs_per_pg.pop(accs_per_pg.index(pg_master)))
                master_row = self.get_master_row_index(pg_df)

            # take abundance for all samples only for master protein
            if with_master_column and master_row is not None:
                all_abundances_per_pg = list(pg_df.iloc[master_row, 2:])
            # no master protein given, mean abundance of all proteins in protein group, ignoring zero values
            else:
                all_abundances_per_pg = list(
                    pg_df.iloc[:, col_index_start_abundances:]
                         .replace(0, np.nan)
                         .mean(axis=0)
                         .replace(np.nan, 0)
                         .astype('float64')
                         .round(3)
                )
            if set(all_abundances_per_pg) == {0}:
                pg_without_abundances.append(pg)
                continue

            samples.extend(sample_list)
            abundance.extend(all_abundances_per_pg)
            proteins.extend([accs_per_pg for i in range(len(sample_list))])

        if pg_without_information > 0:
            print(
                f"{pg_without_information} protein groups without proteins.",
                file=sys.stderr
            )
        if pg_without_abundances:
            print(
                f"{len(pg_without_abundances)} protein groups are without abundance information.",
                file=sys.stderr
            )

        return samples, abundance, proteins

    def read_file_into_dataframe(self):
        df = pd.read_excel(self.result_table, engine="openpyxl")

        df = df.dropna(how='all')
        pg_header_list = df.iloc[1].tolist()
        pos_abundance_list = self.get_abundance_columns(pg_header_list)
        sample_list = self.get_sample_list(pg_header_list, pos_abundance_list)
        xlsx_df = self.reduce_full_df_to_abundance_df(df, pos_abundance_list)
        samples, abundance, proteins = self.create_dict_from_reduced_df(xlsx_df, sample_list)
        # build df from lists
        df_standard = pd.DataFrame(
            {
                'sample': samples,
                'proteins': proteins,
                'quant': abundance
            }
        )

        return self.check_and_adapt_accessions(df_standard)
