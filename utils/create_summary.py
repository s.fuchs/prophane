from utils.summary_classes import Summary


def create_summary(pg_db, task_lca_files, task_lca_support_files, task_map_files, quant_file, mafft_files,
                   output, tasks, sample_groups):
    """
    :param task_lca_files, task_lca_support_files, task_map_files: list of files sorted by task ID
    :param quant_file: 'tasks/quant.tsv'
    :param mafft_files: 'algn/report/mafft.*.txt'
    """
    summary_obj = Summary(pg_db, task_lca_files, task_lca_support_files, task_map_files, quant_file, mafft_files,
                 tasks, sample_groups)
    summary_obj.write_summary_to_output(output)
