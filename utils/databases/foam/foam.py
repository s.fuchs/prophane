from utils.databases.superclasses.hmm_db_superclass import HmmDatabase


class FoamDatabase(HmmDatabase):
    name = "foam"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_map_resource_file(self, *args, **kwargs):
        f = self._config["files"]['map_res']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path
