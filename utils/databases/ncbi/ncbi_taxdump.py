from utils.databases.superclasses.database_superclasses import Database


class NcbiTaxdumpDatabase(Database):
    name = "ncbi_taxdump"
    valid_types = [name]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_taxdump_tar_gz_file(self, *args, **kwargs):
        return self.join_with_db_path(self._config["files"]["taxdump"], *args, **kwargs)
