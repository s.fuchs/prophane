from utils.databases.superclasses.database_superclasses import Database


class NcbiAcc2TaxDatabase(Database):
    name = "ncbi_accession2taxid"
    valid_types = [name]
    mandatory_keys = Database.mandatory_keys + ['files']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_prot_acc2tax_file(self, *args, **kwargs):
        return self.join_with_db_path(self._config["files"]["prot2taxid"], *args, **kwargs)

    def get_all_acc2tax_files(self, *args, **kwargs):
        paths = [self.join_with_db_path(f, *args, **kwargs) for f in self._config["files"].values()]
        return paths
