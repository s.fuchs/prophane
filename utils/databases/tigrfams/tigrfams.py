from utils.databases.superclasses.hmm_db_superclass import HmmDatabase


class TigrfamsDatabase(HmmDatabase):
    name = "tigrfams"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_role_name_file(self, *args, **kwargs):
        f = self._config["files"]['role_name']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path

    def get_role_link_file(self, *args, **kwargs):
        f = self._config["files"]['role_link']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path

    def get_info_tar_file(self, *args, **kwargs):
        f = self._config["files"]['info_tar']
        path = self.join_with_db_path(f, *args, **kwargs)
        return path
