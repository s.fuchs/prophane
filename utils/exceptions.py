#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class DatabaseError(Exception):
    """Base class for database associated errors"""
    pass


class DatabaseOutdatedError(DatabaseError):
    """Exception raised for databases with lower database schema version than required by prophane."""

    def __init__(self, message="Database files are outdated! Please migrate"):
        self.message = message
        super().__init__(self.message)


class DatabaseAccessorKeyError(DatabaseError):
    """Raised if no database is associated with the given keys"""
    def __init__(self, db_accessor, **keys):
        self.db_accessor = db_accessor
        self.keys = keys

    def __str__(self):
        msg = "\n"
        msg += "No database is associated with the specified keys:\n"
        for key, value in self.keys.items():
            msg += f'\t{key}: {value}\n'
        msg += 'Available databases are:\n'
        msg += str(self.db_accessor)
        msg += '\n'
        msg += 'Downloadable databases are:\n'
        msg += "\n".join(sorted([f"- {db}; versions: {', '.join(versions)}"
                                 for (db, versions) in self.db_accessor.get_downloadable_databases()]))
        return msg


class NotAValidDatabaseConfigError(DatabaseError):
    def __init__(self, yaml):
        self.yaml = yaml

    def __str__(self):
        msg = (
            f"Config file: '{self.yaml}'\n"
            + "\tis not a valid database config."
        )
        return msg


class SearchResultParsingError(Exception):
    """
    Raised upon errors during parsing of proteomic search results
    """
    pass


class StyleError(Exception):
    """
    Raised upon input style errors
    """
    pass


class CustomMapError(Exception):
    """
    Raised upon errors during the processing of custom map files in accession to annotation mapping tasks
    """
