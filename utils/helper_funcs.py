import datetime

from pkg_resources import parse_version


def recursively_iter_dict(d):
    for k, v in d.items():
        if isinstance(v, dict):
            recursively_iter_dict(v)
        else:
            yield k, v


def get_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def get_max_version(list_of_version_numbers):
    # pkg_resources do not accept %Y-%m-%d file formats, only year.month; do not accept 15.0 or 'rel1a'
    if len(list_of_version_numbers) == 1:
        return str(list_of_version_numbers[0])
    else:
        list_of_dates = []
        for version in list_of_version_numbers:
            try:
                list_of_dates.append(datetime.datetime.strptime(version, "%Y-%m-%d"))
            except ValueError:
                break

        if list_of_dates:
            v_max = max(list_of_dates).strftime("%Y-%m-%d")
        else:
            v_max = str(max([float(version) for version in list_of_version_numbers]))
        return v_max
